package com.example.rma_projekat_2020271675;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class DetailActivity extends AppCompatActivity {
    private TextView tvName, tvType, tvDescription;
    private DatabaseHelper databaseHelper;
    private Plant plant;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        tvName = findViewById(R.id.tvName);
        tvType = findViewById(R.id.tvType);
        tvDescription = findViewById(R.id.tvDescription);
        databaseHelper = new DatabaseHelper(this);

        int plantId = getIntent().getIntExtra("plant_id", -1);
        plant = databaseHelper.getPlant(plantId);

        if (plant != null) {
            tvName.setText(plant.getName());
            tvType.setText(plant.getType());
            tvDescription.setText(plant.getDescription());
        }

        Button btnEdit = findViewById(R.id.btnEdit);
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailActivity.this, AddEditActivity.class);
                intent.putExtra("plant", plant);
                startActivityForResult(intent, 1);
            }
        });

        Button btnDelete = findViewById(R.id.btnDelete);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                databaseHelper.deletePlant(plant);
                setResult(RESULT_OK);
                finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK) {
            plant = databaseHelper.getPlant(plant.getId());
            if (plant != null) {
                tvName.setText(plant.getName());
                tvType.setText(plant.getType());
                tvDescription.setText(plant.getDescription());
            }
        }
    }
}
