package com.example.rma_projekat_2020271675;


import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class AddEditActivity extends AppCompatActivity {
    private EditText etName, etType, etDescription;
    private DatabaseHelper databaseHelper;
    private Plant plant;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit);

        etName = findViewById(R.id.etName);
        etType = findViewById(R.id.etType);
        etDescription = findViewById(R.id.etDescription);
        databaseHelper = new DatabaseHelper(this);

        if (getIntent().hasExtra("plant")) {
            plant = (Plant) getIntent().getSerializableExtra("plant");
            etName.setText(plant.getName());
            etType.setText(plant.getType());
            etDescription.setText(plant.getDescription());
        }

        Button btnSave = findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                savePlant();
            }
        });
    }

    private void savePlant() {
        String name = etName.getText().toString().trim();
        String type = etType.getText().toString().trim();
        String description = etDescription.getText().toString().trim();

        if (name.isEmpty() || type.isEmpty() || description.isEmpty()) {
            Toast.makeText(this, "Please fill all fields", Toast.LENGTH_SHORT).show();
            return;
        }

        if (plant == null) {
            plant = new Plant(name, type, description);
            databaseHelper.addPlant(plant);
        } else {
            plant.setName(name);
            plant.setType(type);
            plant.setDescription(description);
            databaseHelper.updatePlant(plant);
        }

        setResult(RESULT_OK);
        finish();
    }
}
